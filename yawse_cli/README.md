# YAWSE - CLI Tool

this Dart application is the meat and bones of the system, the one-shot command that "Just Extracts Whatever It Can"(tm), based on whatever runtime configuration it finds, of course.

> NOTE: I'm opting to build this as a separate application for portability reasons; allowing it to run silently and completely headless, without getting in the way of anything the user might be doing.
 
consider a few different potential use cases for this:

* **As a Scheduled Task** - set to trigger on "User Login" and/or "Workstation Unlock", and executed with the `--silent` and `--log-file` runtime flags.
* **As a Triggered Action** - when the System Tray Tool, while monitoring the Windows Spotlight `Assets` folder, detects a new file added.
* **As a User Action** - such as a Start Menu or Desktop shortcut, executed with the `--summary` and `--pause` runtime flags.
