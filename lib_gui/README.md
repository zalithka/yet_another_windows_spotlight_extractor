# YAWSE - Graphical User Interface Objects

this Flutter package exports widgets that are used in rendering the application popup windows, which in turn provide easy access to edit runtime configuration options.

this will cater for the following two primary windows:

* **Runtime Configuration** - which will be launched from a dedicated executable, (optionally) with it's own Start/Desktop shortcut.
* **Tray Configuration** - which will be launched from an option in the System Tray Tool, providing access to tray-specific runtime configuration.
