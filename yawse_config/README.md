# YAWSE - Configuration Tool

this Flutter application is used solely to edit runtime configuration options in the current user environment.

> NOTE: one day, this may also contain additional pages for things like in-app update checks, configuration backup and restore, user feedback form, etc.. these points are currently undecided.

this will allow the following actions to be performed on all runtime configuration options:

* **Clear Runtime Setting** - will remove the custom value, causing the next runtime to fall back to internal defaults.
* **Update Runtime Setting** - will prompt for appropriate user input based on the setting being updated.
