# Yet _Another_ Windows Spotlight Extractor

this monorepo contains a few projects, each designed to be coded and built independently of one another, but ultimately all running within the same "space".

## Workspace Projects

each of the projects found here live to serve a very specific purpose, with code from each library being used in multiple applications.

> NOTE: I have looked into `melos` for the multi-package story, but it doesn't quite fit my requirement for _two_ Flutter apps... I'm open to suggestions here. :/

### Extractor Runtime Business Logic `lib_extractor`

this Dart package exports structured methods with the logic to _actually extract_ the wallpapers, governed by configuration options in the current runtime environment.

where this "runtime configuration" is built up from the following layers:

* **Internal Defaults** - these are selected in a way that provides the best value for money, but may not work for everybody.
* **User Configuration** - which exist within text-based files in your user environment, read and updated by the various components at runtime.
* **Runtime Arguments** - at least in the case of the CLI tool, any configuration option may be explicitly overridden for _only that runtime_.

> [View Project](./lib_extractor/)

### Graphical User Interface Objects `lib_gui`

this Flutter package exports widgets that are used in rendering the application popup windows, which in turn provide easy access to edit runtime configuration options.

this will cater for the following two primary windows:

* **Runtime Configuration** - which will be launched from a dedicated executable, (optionally) with it's own Start/Desktop shortcut.
* **Tray Configuration** - which will be launched from an option in the System Tray Tool, providing access to tray-specific runtime configuration.

> [View Project](./lib_gui/)

### User Environment Convenience Wrapper `lib_system`

this Dart package exports structured methods for interacting with the current system host, providing a means to retrieve relevant information from host system resources.

for the sake of transparency, this includes the following sources:

* **Windows Registry** - from which the current user's "My Pictures" path is read, because this can be customised on the host.
* **User AppData Folder** - because this is where the source images are saved by Windows.
* **User Profile Folder** - because this is where the YAWSE configuration file will live.

on that last note, the current runtime settings include:

* **Output Folder** (folder path) - is the target folder to use when extracting wallpapers.
* **Filter Orientation** (select list) - select either `landscape` or `portrait` to restrict the extractor output to.
  * when not set, this will extract both.
  * _NOTE: using this option implies `--flatten`, as you probably don't want a `landscape` or `portrait` subfolder if you're only extracting one size..._
* **Flatten Output** (toggle) - whether or not to flatten the extractor output.
  * when enabled, this prevents the creation of the `landscape` and `portrait` subfolders.
  * _NOTE: when used without an orientation filter, this will cause both `landscape` and `portrait` wallpapers to be placed in the same folder, which you probably don't want..._
* **Runtime Logging** (folder path) - is the folder to use to write runtime log files.
  * _NOTE: these are written using rolling appenders, with individual file sizes limited to `5mb` and automatically deleted at 1 week old, because **nobody** likes big log folders... more config options may come in the future._

> [View Project](./lib_system/)

### CLI Tool `yawse_cli`

this Dart application is the meat and bones of the system, the one-shot command that "Just Extracts Whatever It Can"(tm), based on whatever runtime configuration it finds, of course.

> NOTE: I'm opting to build this as a separate application for portability reasons; allowing it to run silently and completely headless, without getting in the way of anything the user might be doing.
 
consider a few different potential use cases for this:

* **As a Scheduled Task** - set to trigger on "User Login" and/or "Workstation Unlock", and executed with the `--silent` and `--log-file` runtime flags.
* **As a Triggered Action** - when the System Tray Tool, while monitoring the Windows Spotlight `Assets` folder, detects a new file added.
* **As a User Action** - such as a Start Menu or Desktop shortcut, executed with the `--summary` and `--pause` runtime flags.

> [View Project](./yawse_cli/)

### Configuration Tool `yawse_config`

this Flutter application is used solely to edit runtime configuration options in the current user environment.

> NOTE: one day, this may also contain additional pages for things like in-app update checks, configuration backup and restore, user feedback form, etc.. these points are currently undecided.

this will allow the following actions to be performed on all runtime configuration options:

* **Clear Runtime Setting** - will remove the custom value, causing the next runtime to fall back to internal defaults.
* **Update Runtime Setting** - will prompt for appropriate user input based on the setting being updated.

> [View Project](./yawse_config/)

### System Tray Tool `yawse_tray`

this Flutter application runs in the current user session, with a System Tray icon pinning it to the Shell, and a context menu for feature access.

the context menu options include:

* **Runtime Configuration** - which will show the standard Configuration Tool window, plus a few extra options specifically for the System Tray application.
* **View Log Folder** - which will open the configured log file folder in Explorer. if none is configured, a message will be shown to say so.
* **Run Extractor** - which will launch the CLI Tool.
* **Exit** - which will shut down the System Tray Tool.

as for the System Tray Tool extra options, because this application is designed to run in the "background", it makes use of this persistent state by monitoring the Windows Spotlight `Assets` folder. this provides a mechanism to trigger the extraction of wallpaper images, effectively as soon as they're downloaded..

because this process will happen silently in the background, an option will be provided to show a desktop notification when this happens. when enabled, this will show a basic summary of how many images were found and extracted after it completes.

> [View Project](./yawse_tray/)
