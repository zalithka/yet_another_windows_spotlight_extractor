import 'package:yet_another_windows_spotlight_extractor/yawse_cli.dart';
import 'package:test/test.dart';

void main() {
  group('run a test that..', () {
    test('does nothing at all..', () {
      expect('ping', 'ping');
    });
  });
}
