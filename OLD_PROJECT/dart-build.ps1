# this simplifies the process of compiling an executable binary

try {
    dart compile exe -o .\release\yawse.exe .\bin\yawse_cli.dart
} catch {
	## If, at any time, for any code inside of the try block, returns a hard-terminating error
	## PowerShell will divert the code to the catch block which writes to the console
	## and exits the PowerShell console with a 1 exit code.
	Write-Host "Error: $($_.Exception.Message)"
	exit 1
}

## When the code inside of the try/catch/finally blocks completes (error or not),
## exit the PowerShell session with an exit code of 0
exit 0
