import 'package:dcli/dcli.dart';
import 'package:loggy/loggy.dart';

void initLoggyFromRuntimeFlags(ArgResults? globalResults) {
  LoggyPrinter logPrinter = PrettyPrinter(showColors: true);
  LogLevel logLevel = LogLevel.info;
  LogLevel stackTraceLevel = LogLevel.off;

  if (globalResults != null && globalResults.wasParsed('silent')) {
    logDebug('parsed global "silent" CLI flag: ${globalResults['silent']}');
    logLevel = LogLevel.off;
  }

  if (globalResults != null && globalResults.wasParsed('verbose')) {
    logDebug('parsed global "verbose" CLI flag: ${globalResults['verbose']}');
    logLevel = LogLevel.all;
    stackTraceLevel = LogLevel.error;
  }

  LogOptions logOptions = LogOptions(logLevel, stackTraceLevel: stackTraceLevel);

  Loggy.initLoggy(
    logPrinter: logPrinter,
    logOptions: logOptions,
  );
}
