// ! this file contains helper classes for manipulating the Spotlight Extractor's configuration objects.

import 'dart:convert';
import 'dart:io';

import 'package:dcli/dcli.dart';
import 'package:loggy/loggy.dart';
import 'package:win32_registry/win32_registry.dart';
import 'package:yet_another_windows_spotlight_extractor/models/runtime_config.dart';

final Directory dirSpotlightAssets = Directory(join(
  env["LOCALAPPDATA"]!,
  r'Packages\Microsoft.Windows.ContentDeliveryManager_cw5n1h2txyewy\LocalState\Assets',
));

final Directory dirYawseConfig = Directory(join(env['USERPROFILE']!, r'.yawse'));
final File fileYawseConfig = File(join(dirYawseConfig.path, r'config.json'));
final File fileExtractedList = File(join(dirYawseConfig.path, r'extracted'));

bool get userlandConfigExists {
  try {
    _getUserlandConfig();
    logDebug('explicit userland config existence check passed.');
    return true;
  } catch (error) {
    logDebug('explicit userland config existence check failed.');
    return false;
  }
}

/// this is intended explicitly for generating a RuntimeConfig instace from
RuntimeConfig expandArgsToEffectiveConfig(RuntimeConfig argsConfig) {
  logDebug('expandArgsToEffectiveConfig(RuntimeConfig argsConfig): "${argsConfig.name}"');

  // first, create a placeholder with internal defaults..
  RuntimeConfig expandedConfig = _generateCoreRuntimeConfig();

  try {
    // see if we have userland config..
    expandedConfig = RuntimeConfig.merge(expandedConfig, _getUserlandConfig());
  } catch (error) {
    logWarning(error.toString());
  }

  try {
    // apply the provided runtime arguments config..
    expandedConfig = RuntimeConfig.merge(expandedConfig, argsConfig);
  } catch (error) {
    logError(error.toString());
  }

  // ..and return a new RuntimeConfig instance
  return expandedConfig;
}

/// update the userland runtime configuration file with the provided RuntimeConfig instance
///
/// this will fail if the config file does not exist yet.
void writeUserlandConfig(RuntimeConfig config, {bool? force}) {
  if ((!userlandConfigExists && force != true) || (userlandConfigExists && force != true)) {
    throw 'this function is not currently allowed to write changes.';
  }

  // ! for this to be truly useful, it should show a summary of what will change...
  // let's first rename this config though, custom named configs will come one day
  logDebug('renaming RuntimeConfig from "${config.name}" to "user". because reasons...');
  config.name = 'user';

  fileYawseConfig.writeAsStringSync(config.toString(), flush: true);

  return logDebug('userland config file successfully updated');
}

/// remove the current userland configuration file
void clearUserRuntimeConfig() {
  if (!userlandConfigExists) {
    return logInfo('userland config file does not exist, nothing to remove..');
  }

  fileYawseConfig.deleteSync();

  return logDebug('userland config file successfully removed');
}

RuntimeConfig? _getUserlandConfig() {
  logDebug('looking for userland config file..');

  if (!userlandConfigExists) {
    throw 'userland config does not exist yet..';
  }

  final String yawseConfigString = fileYawseConfig.readAsStringSync();
  return RuntimeConfig.fromJson(yawseConfigString);
}

RuntimeConfig _generateCoreRuntimeConfig() {
  logDebug('generating a RuntimeConfig instance with internal defaults..');

  // initialise the core runtime configuration with internal defaults..
  RuntimeConfig coreConfig = RuntimeConfig(
    'core',
    assetsPath: _getSpotlightAssetsPath(),
    destination: _ensureAbsolutePicturesPath('Spotlight Wallpapers'),
  );

  return coreConfig;
}

String _getSpotlightAssetsPath() {
  logDebug('looking for the Windows Spotlight assets path..');

  if (!dirSpotlightAssets.existsSync()) {
    throw 'assets folder path not found.. do you have Windows Spotlight enabled in your profile?';
  }

  return dirSpotlightAssets.path;
}

/// relative paths will be appended to the user's current "My Pictures" folder path
///
/// for the sake of script flow, if null is passed here, this will give null back in return..
String? _ensureAbsolutePicturesPath(String? inputPath) {
  logDebug('attempting to make a provided path absolute: "$inputPath"');

  if (inputPath == null) {
    logDebug('_ensureAbsolutePath() called with null..');
    return inputPath;
  }

  String targetFolder = normalize(inputPath);

  // first, if the path is already absolute, then just return it as is..
  if (isAbsolute(targetFolder)) {
    return targetFolder;
  }

  // otherwise, append it to the user's "My Pictures" folder
  String userlandMyPicturesPath = _getUserlandMyPicturesPath();
  return join(userlandMyPicturesPath, targetFolder);
}

String _getUserlandMyPicturesPath() {
  logDebug('looking for userland "My Pictures" path in Windows Registry..');

  String? userPicturesPath = Registry.openPath(
    RegistryHive.currentUser,
    path: r'SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders',
  ).getValueAsString('My Pictures');

  if (userPicturesPath == null) {
    throw 'could not locate path to current user "My Pictures" folder, are you running Windows 10?';
  }

  return userPicturesPath;
}
