import 'dart:io';

import 'package:args/command_runner.dart';
import 'package:dcli/dcli.dart';
import 'package:loggy/loggy.dart';
import 'package:yet_another_windows_spotlight_extractor/helpers/config.dart';
import 'package:yet_another_windows_spotlight_extractor/helpers/logging.dart';
import 'package:yet_another_windows_spotlight_extractor/models/runtime_config.dart';

class ConfigCommand extends Command {
  @override
  final name = 'config';

  @override
  final description = "Manage the Spotlight Extractor runtime configuration options.";

  ConfigCommand() {
    argParser
      ..addSeparator('Special Config Actions:')
      ..addFlag(
        'reset',
        defaultsTo: false,
        negatable: false,
        help: 'remove the entire YAWSE configuration folder and all its files.',
      )
      ..addFlag(
        'install',
        negatable: false,
        help:
            'make this CLI tool available throughout your environment.\n[1] copy this CLI tool binary to "%APPDATA%\\yet-another-windows-spotlight-extractor"\n[2] add this folder to your user environment "PATH" variable.',
        hide: true,
      )
      ..addFlag(
        'uninstall',
        negatable: false,
        help: 'remove any and all installed files in the current environment.',
        hide: true,
      )
      ..addSeparator('Runtime Configuration Options:\n[*] these will be ignored if any of the "Special Config Actions" flags are used.')
      ..addOption(
        'destination',
        abbr: 'd',
        valueHelp: 'PATH',
        help: 'a path to copy the extracted wallpapers to.\n[*] non-absolute paths are calculated relative to your current user "My Pictures" folder.',
      )
      ..addOption(
        'orientation',
        abbr: 'o',
        valueHelp: 'OPTION',
        allowed: ['landscape', 'portrait'],
        help: 'limit the extract process to only the specified wallpaper orientation.',
      )
      ..addFlag(
        'flatten',
        abbr: 'f',
        help: 'when set, this prevents the creation of "landscape" and "portrait" subfolders.',
      )
      ..addFlag(
        'overwrite',
        abbr: 'w',
        help: 'when set, causes the file extractor to overwrite destination filenames if they already exist',
      )
      ..addFlag(
        'pause',
        abbr: 'p',
        negatable: false,
        help: 'when set, the file extractor will remain open after it has run, showing a summary of what it did.',
      );
  }

  @override
  Future<void> run() async {
    initLoggyFromRuntimeFlags(globalResults);

    if (argResults!.wasParsed('reset')) {
      logDebug('parsed local "reset" CLI flag: ${argResults?['reset']}');

      return clearUserRuntimeConfig();
    }

    String? argsDestination;
    String? argsOrientation;
    bool? argsFlatten;

    // collect any provided runtime configuration options..
    if (argResults!.wasParsed('destination')) {
      logDebug('parsed destination option: ${argResults?['destination']}');
      argsDestination = argResults?['destination'];
    }
    if (argResults!.wasParsed('orientation')) {
      logDebug('parsed orientation option: ${argResults?['orientation']}');
      argsOrientation = argResults?['orientation'];
    }
    if (argResults!.wasParsed('flatten')) {
      logDebug('parsed flatten flag: ${argResults?['flatten']}');
      argsFlatten = argResults?['flatten'];
    }

    bool haveArgsInput = [argsDestination, argsOrientation, argsFlatten].any((element) => element != null);

    if (!haveArgsInput) {
      logWarning('note:the  interactive mode for this command is current a work in progress, please call it directly to customise config.');
    }

    // create a new RuntimeConfig using the parsed argument options
    RuntimeConfig effectiveConfig = expandArgsToEffectiveConfig(RuntimeConfig(
      'runtime',
      destination: argsDestination,
      orientation: argsOrientation,
      flatten: argsFlatten,
    ));
    logInfo('effective runtime config: $effectiveConfig');

    // this value is preemptively false, pending further conditional logic..
    bool mustWriteConfig = false;

    // prompt user to save, if any config values were customised..
    if (haveArgsInput && confirm('you have customised config values, would you like to save these as default?', defaultValue: true)) {
      mustWriteConfig = true;
    }

    // prompt user to save, only if the user config file does not exist yet and no config values were customised
    if (!haveArgsInput &&
        !userlandConfigExists &&
        confirm('no user configuration file was found, would you like to save this config as default?', defaultValue: true)) {
      mustWriteConfig = true;
    }

    if (mustWriteConfig) {
      if (globalResults?['dry-run'] == true) {
        logInfo('dry run set, skipping the user config file write.');
      } else {
        writeUserlandConfig(effectiveConfig, force: !userlandConfigExists);
      }
    }
  }
}
